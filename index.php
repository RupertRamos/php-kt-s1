
<!-- Used to connect code.php to index.php -->
<!-- can use inclue or import (research on this) -->
<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>s01: PHP Basics and Selection Control Structures</title>

</head>
<body>

	<!-- <h1>Hello World!</h1> -->

	<h1>Echoing Values</h1>

	<p>
		<?php echo 'Good day $name! Your given email is $email' ?>
	</p>

	<!-- ctrl + shift + d -->
	<p>
		<?php echo "Good day $name! Your given email is $email" ?>
	</p>


	<p>
		<?php echo PI?>
	</p>

	<h1>Data Types</h1>
	<p>
		<?php echo $address ?>
		<?php echo $age ?>
	</p>

	<!-- To output the value of an object property, the arrow notation can be used.  -->

	<p>
		<?php echo $gradesObj -> firstGrading;?>
		<?php echo $personObj -> address -> state;?>
	</p>


	<!-- Normal echoing of null and boolean var will not make it visible to the web page -->
	<!-- Will not output boolean/null -->
	<p><?php echo $hasTravelledAbroad; ?></p>
	<p><?php echo $girlfriend; ?></p>


	<!-- var_dump() allows us to see more details/info on the variable -->
	<p><?php echo gettype($hasTravelledAbroad); ?></p>
	<p><?php echo var_dump($hasTravelledAbroad); ?></p>
	<p><?php echo var_dump($girlfriend); ?></p>

	<p><?php echo $grades[2] ?></p>


	<h1>Operators</h1>

	<h2>Arithmetic Operators</h2>

	<p>Sum: <?php echo $x + $y ?> </p>
	<p>Minus: <?php echo $x - $y ?> </p>
	<p>Multiply: <?php echo $x * $y ?> </p>
	<p>Div: <?php echo $x / $y ?> </p>
	<p>Modulo: <?php echo $x % $y ?> </p>

	<h2>Equality Operators</h2>

	<p>Loose Equality: <?php echo var_dump($x == '1342.14')?></p>
	<p>Loose Equality: <?php echo var_dump($x == 1342.14)?></p>
	<p>Loose Equality: <?php echo var_dump($x != '1342.14')?></p>


	<h2>Greater/Lesser Operators</h2>

	<p>isGreater: <?php echo var_dump($x > $y)?></p>
	<p>isLesser: <?php echo var_dump($x < $y)?></p>

	<h2>Logical Operators</h2>

	<p>Are all requirements met: <?php echo var_dump($isLegalAge && $isRegistered) ?></p>
	<p>Are all requirements met: <?php echo var_dump($isLegalAge && !$isRegistered) ?></p>
	<p>Are all requirements met: <?php echo var_dump(!$isLegalAge && !$isRegistered) ?></p>

	<h1>Function</h1>

	<p>Full Name: <?php echo getFullName('John', 'D','Smith'); ?></p>
	<p>Full Name: <?php echo getFullName(); ?></p>
	<p>User Name: <?php echo getUserName(); ?></p>


	<h2>If-ElseIf-Else</h2>

	<p><?php echo determineTyphoonIntensity(35);?></p>

	<h2>Ternary Operator</h2>
	<p>78: <?php echo var_dump(isUnderAge(78))?></p>
	<p>17: <?php echo var_dump(isUnderAge(17))?></p>

	<h2>Switch Statements</h2>

	<p><?php echo determineComputerUser(4)?></p>
	<p><?php echo determineComputerUser(6)?></p>

	<h2>Try Catch Finally</h2>

	<p><?php echo greeting("hi")?></p>
	<p><?php echo greeting(4)?></p>


	<h3>S1 Activity</h3>
	<!-- getFullAddress Function -->
	<p><?php echo getFullAddress("3F Caswynn Bldg., Timgog Avenue", " Quezon City", "Metro Manila", "Philippines")?></p>
	<p><?php echo getFullAddress("3F Enzo Bldg, Buendia Avenue", " Makati City", "Metro Manila", "Philippines")?></p>

	<!-- getLetterGrade Function -->
	<p>87 is equivalent to <?php echo getLetterGrade(87)?></p>
	<p>94 is equivalent to <?php echo getLetterGrade(94)?></p>
	<p>74 is equivalent to <?php echo getLetterGrade(74)?></p>

</body>
</html>