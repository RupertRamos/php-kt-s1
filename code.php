<?php 
	
// Variables
// Variables are defined/declared using the dollar ($) notation before the name of the variable.
$name = 'John Smith';
$email = 'johnsmith@gmail.com';


// Constants
define('PI', 3.1416);


// Data Types

// Strings
$state = 'New York';
$country = 'United States of America';

// $address = $state.', ' .$country; // Concatination of strings via the dot sign

$address = "$state, $country"; // Concatination via double qoutes

// Integers
$age = 31;
$headcount = 26;

// Decimals
$grade = 98.2;
$distanceInKilometers = 1324.34;

// Boolean
$hasTravelledAbroad = false;
$haveSymptoms = true;


// Null
$boyfriend = null;
$girlfriend = null;


// Arrays
$grades = array(90.1, 87, 98, 93);

// Objects
$gradesObj = (object) [
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 95
];

$personObj = (object) [
	'fullName' => 'John Doe',
	'isMarried' => false,
	'age' => 35,
	'address' => (object) [
		'state' => 'New York',
		'country' => 'United States of America'
	]
];


// Operators

$x = 1342.14;
$y = 1268.24;

$isLegalAge = true;
$isRegistered = false;


// Functions

function getFullName($firstName = "User", $middleInitial = "User", $lastName = "User") {
	return "$lastName, $firstName, $middleInitial";
};

// Default Value has to be provided in each parameters
function getUserName($userName = "User") {
	return "$userName";
};

// getFullName(); // call-back function in JS;


// Selection Control Structures

// If-ElseIf-Else Statement

function determineTyphoonIntensity($windspeed) {
	if ($windspeed < 30) {
		return 'Not a typhoon yet';
	} else if ($windspeed <= 61) {
		return 'Tropical depression detected';
	} else if ($windspeed >= 62 && $windspeed <= 88) {
		return 'Tropical Storm detected';
	} else if ($windspeed >= 89 && $windspeed <= 177) {
		return 'Severe Tropical Storm detected';
	} else {
		return 'Typhoon detected';
	}
};


// Conditional (Ternary) Operator

function isUnderAge($age) {
	return ($age < 18) ? true : false;
};

// Switch Statement:

function determineComputerUser($computerNumber) {
	switch ($computerNumber) {
		case 1:
			return 'Linus Torvalds';
			break;
		case 2:
			return 'Steve Jobs';
			break;
		case 3:
			return 'Sid Meier';
			break; 
		case 4:
			return 'Onel de Guzman';
			break;
		case 5: 
			return 'Christian Salvador';
			break;
		default:
			return $computerNumber. ' is out of bounds.';
			break;
	}
};


// Try-Catch-Finally Statement
function greeting($str) {
	try{
		// Attempt to execute a code.

		if(gettype($str) == "string") {
			echo $str;
		} else {
			throw new Exception("Ooooooppsss!");
		}
	}
	catch (Exception $e) {
		// Catch errors within "try" code block
		echo $e -> getMessage();
	}
	finally {
		// continue of execution of code regardless of success or failure of code execution.

		echo " I did it again!";
	}
}


// S1 Activity:

function getFullAddress($streetAdd, $city, $province, $country) {
	return "$streetAdd, $city, $province, $country";
};


function getLetterGrade($grade) {
	if($grade <= 75) {
		return 'F';
	} else if ($grade >= 76 && $grade <= 75) {
		return 'C-';
	} else if ($grade >= 77 && $grade <= 79) {
		return 'C';
	} else if ($grade >= 80 && $grade <= 82) {
		return 'C+';
	} else if ($grade >= 83 && $grade <= 85) {
		return 'B-';
	} else if ($grade >= 86 && $grade <= 88) {
		return 'B';
	} else if ($grade >= 89 && $grade <= 91) {
		return 'B+';
	} else if ($grade >= 92 && $grade <= 94) {
		return 'A-';
	} else if ($grade >= 95 && $grade <= 97) {
		return 'A';
	} else if ($grade >= 98 && $grade <= 100) {
		return 'A+';
	};
};



?>